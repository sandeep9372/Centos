#!/bin/bash

rm -f /var/lib/rpm/__db*
if [ $? -eq 0 ]; then
  echo -ne "\n\tsuccessful removal of the database files\n"
else
  echo -ne "\terror deleting the database files\n\tcode: $?\n"
fi
echo -ne "\t$(db_verify /var/lib/rpm/Packages)\n"
rpm --rebuilddb
if [ $? -eq 0 ];then
  echo -ne "\tRebuilding Database\n"
else
  echo -ne "\tRebuilding Failed\n"
fi
yum clean all > /dev/null
echo -ne "\tAll repos cache cleaned\n\n\tDone!\n\n"

